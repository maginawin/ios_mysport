//
//  WMSetViewController.h
//  MySport
//
//  Created by maginawin on 15/3/30.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMSetElements.h"

@interface WMSetViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
