//
//  PedoTableViewController.m
//  MySport
//
//  Created by maginawin on 15-1-22.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import "PedoTableViewController.h"
#import "WWDTools.h"

#define NOTI_VALUE @"noti_value" //正常模式下的数据

@interface PedoTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pedoLabel;
@property (weak, nonatomic) IBOutlet UILabel *calorieLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrSetLabel;
@property (weak, nonatomic) IBOutlet UILabel *rpmLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pedosLabel;
@property (weak, nonatomic) IBOutlet UILabel *distLabel;

@property (nonatomic) BOOL isHaveDist;

@end

@implementation PedoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.allowsSelection = NO;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiValue:) name:NOTI_VALUE object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    WMPedoElements* setElements = [[WMPedoElements alloc] init];
    _hrSetLabel.text = setElements.mHR;
}

#pragma mark - Notificaiton selector

- (void)notiValue:(NSNotification*)noti {
    NSString* value = [noti object];
    NSString* tag = [WWDTools stringFromIndexCount:0 count:2 from:value];
    // 计步
    if ([@"F8" isEqualToString:tag]) {
        // 2, 3 字节表示车表 minutes, seconds
        NSString* timeStr = [WWDTools stringFromIndexCount:2 count:4 from:value];
        _timeLabel.text = [self getMinutesSecondsFromHexStr:timeStr];
        // 4 字节为 00 表示一个信号计一步,无 dist (single), 01 表示两个信号计一步, 无 dist (double) 02 表示 一个信号计一步, 有 dist 03 表示 两个信号计一步, 有dist
        NSString* modeStr = [WWDTools stringFromIndexCount:6 count:2 from:value];
        _modeLabel.text = [self getModeFromHexStr:modeStr];
        // 5, 6, 7 字节表示车表 CAL (0.0)
        NSString* calStr = [WWDTools stringFromIndexCount:8 count:6 from:value];
        _calorieLabel.text = [self getCalFromHexStr:calStr];
        // 8, 9 字节表示车表 TCNT
        NSString* tcntStr = [WWDTools stringFromIndexCount:14 count:4 from:value];
        _pedosLabel.text = [self getIntStrFromHexStr:tcntStr];
        // 10, 11 字节表示车表 RPM
        NSString* rpmStr = [WWDTools stringFromIndexCount:18 count:4 from:value];
        _rpmLabel.text = [self getIntStrFromHexStr:rpmStr];
        // 12, 13 字节表示车表 CNT
        NSString* cntStr = [WWDTools stringFromIndexCount:22 count:4 from:value];
        _pedoLabel.text = [self getIntStrFromHexStr:cntStr];
        // 14, 15, 16 字节表示 dist 0.00
        NSString* distStr = [value substringWithRange:NSMakeRange(26, 6)];
        _distLabel.text = [self getDistFromHexStr:distStr];
    } else if([@"F9" isEqualToString:tag]){
        dispatch_async(dispatch_get_main_queue(), ^ {
            //heart rate value
            NSString* hrValueStr = [WWDTools stringFromIndexCount:2 count:4 from:value];
            NSUInteger hrValueInt = [WWDTools intFromHexString:hrValueStr];
            NSString* hrString = [NSString stringWithFormat:@"%lu", (unsigned long)hrValueInt];
            _hrLabel.text = hrString;
            //heart rate set
            NSString* hrSetStr = [WWDTools stringFromIndexCount:6 count:4 from:value];
            NSUInteger hrSetInt = [WWDTools intFromHexString:hrSetStr];
            NSString* hrSetString = [NSString stringWithFormat:@"%lu", (unsigned long)hrSetInt];
            _hrSetLabel.text = hrSetString;
        });
    }
    // 计步倒计
    /*
    else if ([@"FE" isEqualToString:tag]) {
        
    }
    // 心率
    else if ([@"F9" isEqualToString:tag]) {
    
    }
     */
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 8;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nil" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tool methods

// 传进 hexStr 为 2 个字节的十六进制字符串, 返回 00 : 00 格式的 M : S 时间字符串
- (NSString*)getMinutesSecondsFromHexStr:(NSString*)hexStr {
    NSString* mHexStr = [WWDTools stringFromIndexCount:0 count:2 from:hexStr];
    NSString* sHexStr = [WWDTools stringFromIndexCount:2 count:2 from:hexStr];
    NSUInteger mInt = [WWDTools intFromHexString:mHexStr];
    NSUInteger sInt = [WWDTools intFromHexString:sHexStr];
    NSString* mStr = @"";
    NSString* sStr = @"";
    if (mInt < 10) {
        mStr = [NSString stringWithFormat:@"0%lu", (unsigned long)mInt];
    } else {
        mStr = [NSString stringWithFormat:@"%lu", (unsigned long)mInt];
    }
    if (sInt < 10) {
        sStr = [NSString stringWithFormat:@"0%lu", (unsigned long)sInt];
    } else {
        sStr = [NSString stringWithFormat:@"%lu", (unsigned long)sInt];
    }
    return [NSString stringWithFormat:@"%@ : %@", mStr, sStr];
}

// 传进 hexStr 为 1 个字节的十六进制字符串, 返回 modeStr 为 signle 或 double 的 mode
- (NSString*)getModeFromHexStr:(NSString*)hexStr {
    if ([@"01" isEqualToString:hexStr]) {
        _isHaveDist = NO;
        return @"双信号";
    } else if ([@"02" isEqualToString:hexStr]) {
        _isHaveDist = YES;
        return @"单信号";
    } else if ([@"03" isEqualToString:hexStr]) {
        _isHaveDist = YES;
        return @"双信号";
    } else {
        _isHaveDist = NO;
        return @"单信号";
    }
}

// 传进 hexStr 为 3 个字节的十六进制字符串, 返回 calStr 为十进制卡路里字符串带一位小数
- (NSString*)getCalFromHexStr:(NSString*)hexStr {
    NSUInteger calInt = [WWDTools intFromHexString:hexStr];
    float calFloat = calInt / 10.00;
    return [NSString stringWithFormat:@"%.1f", calFloat];
}

// 传进 hexStr 为 2 or 3 个字节的十六进制字符串, 返回 pedos, pedo, cnt 的字符串值
- (NSString*)getIntStrFromHexStr:(NSString*)hexStr {
    NSUInteger tcntInt = [WWDTools intFromHexString:hexStr];
    return [NSString stringWithFormat:@"%ld", (unsigned long)tcntInt];
}

- (NSString*)getDistFromHexStr:(NSString*)distHexStr {
    if (_isHaveDist) {
        NSInteger distInt = [WWDTools intFromHexString:distHexStr];
        float distF = distInt / 100.000;
        return [NSString stringWithFormat:@"%.2f", distF];
    } else {
        return @"- -";
    }
}

@end
