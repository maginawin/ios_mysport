//
//  WMSetViewController.m
//  MySport
//
//  Created by maginawin on 15/3/30.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import "WMSetViewController.h"

#define NOTI_VALUE @"noti_value" //正常模式下的数据

@interface WMSetViewController ()

@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (nonatomic, strong) WMSetElements* setElements;

@property (strong, nonatomic) UILabel* timeLabel;
@property (strong, nonatomic) UILabel* distanceLabel;
@property (strong, nonatomic) UILabel* countStepsLabel;
@property (strong, nonatomic) UILabel* calorieLabel;
@property (strong, nonatomic) UILabel* heartRateLabel;
@property (strong, nonatomic) UILabel* distanceUnitLabel;
@property (strong, nonatomic) UISegmentedControl *setModeChange;

@property (strong, nonatomic) NSMutableArray *upButtons;
@property (strong, nonatomic) NSMutableArray *downButtons;

@property (nonatomic) BOOL isTouchDown;
@property (nonatomic) float repeatInterval;

@property (nonatomic) BOOL isSending;

@end

@implementation WMSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    _mTableView.tableFooterView = [[UIView alloc] init];
    _setElements = [[WMSetElements alloc] init];
    _isSending = NO;
    _upButtons = [[NSMutableArray alloc] init];
    _downButtons = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _isTouchDown = NO;
    _repeatInterval = 0.05;
    
    [self reviewDefaultValues];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _isTouchDown = NO;
    _repeatInterval = 0.05;
}

#pragma mark - IBAction about

- (IBAction)shortClick:(UIButton*)sender {
    NSInteger senderTag = sender.tag;
    if (senderTag == 11) {
        _timeLabel.text = [_setElements downTime];
    } else if (senderTag == 12) {
        _timeLabel.text = [_setElements upTime];
    } else if (senderTag == 21) {
        _distanceLabel.text = [_setElements downDistance];
    } else if (senderTag == 22) {
        _distanceLabel.text = [_setElements upDistance];
    } else if (senderTag == 31) {
        _countStepsLabel.text = [_setElements downCountSteps];
    } else if (senderTag == 32) {
        _countStepsLabel.text = [_setElements upCountSteps];
    } else if (senderTag == 41) {
        _calorieLabel.text = [_setElements downCalorie];
    } else if (senderTag == 42) {
        _calorieLabel.text = [_setElements upCalorie];
    } else if (senderTag == 51) {
        _heartRateLabel.text = [_setElements downHeartRate];
    } else if (senderTag == 52) {
        _heartRateLabel.text = [_setElements upHeartRate];
    }
}

- (IBAction)sendSetElements:(id)sender {
    if (!_isSending) {
        _isSending = YES;
        [_setElements writeSetElementsToPeripheral];
        [self setDefaultValues];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"正在设置, 请稍候..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert setMessage:@"设置完成"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [alert dismissWithClickedButtonIndex:0 animated:YES];
                _isSending = NO;
            });
        });
    }
}

- (IBAction)setModeChanged:(id)sender {
    
}

#pragma mark - Table view about

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"idCell%d", (int)row] forIndexPath:indexPath];
    UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    longPressGesture.minimumPressDuration = 2;
    [(UIButton*)[tableView viewWithTag:11 + row * 10] addGestureRecognizer:longPressGesture];
    UILongPressGestureRecognizer* longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [(UIButton*)[tableView viewWithTag:12 + row * 10] addGestureRecognizer:longPressGesture1];
    if (row == 0) {
        _timeLabel = (UILabel*)[tableView viewWithTag:10];
    } else if (row == 1) {
        _distanceLabel = (UILabel*)[tableView viewWithTag:20];
        _distanceUnitLabel = (UILabel*)[tableView viewWithTag:23];
    } else if (row == 2) {
        _countStepsLabel = (UILabel*)[tableView viewWithTag:30];
    } else if (row == 3) {
        _calorieLabel = (UILabel*)[tableView viewWithTag:40];
    } else if (row == 4) {
        _heartRateLabel = (UILabel*)[tableView viewWithTag:50];
    } else if (row == 5) {
        _setModeChange = (UISegmentedControl*)[tableView viewWithTag:60];
        [_setModeChange addTarget:self action:@selector(setModeValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    
    return cell;
}

- (void)longPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        _isTouchDown = YES;
        NSInteger senderTag = sender.view.tag;
        [self longPressChangeViewWithTag:senderTag];
        NSLog(@"begin");
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"end");
        _isTouchDown = NO;
        _repeatInterval = 0.05;
    }
}

- (void)setModeValueChanged:(UISegmentedControl*)sender {
    if (sender.selectedSegmentIndex == 0) {
        _setElements.setMode = SET_MODE_SPEED;
        _distanceUnitLabel.text = [_setElements defaultDistanceUnit];
    } else {
        _setElements.setMode = SET_MODE_PEDO;
        _distanceUnitLabel.text = @"千米";
    }
}

// 长按改变视图的方法
- (void)longPressChangeViewWithTag:(NSInteger)senderTag {
    if (_isTouchDown) {
        dispatch_queue_t repeatQueue = dispatch_queue_create("WMSetViewController.repeatQueue", DISPATCH_QUEUE_SERIAL);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_repeatInterval * NSEC_PER_SEC)), repeatQueue, ^{
            dispatch_async(dispatch_get_main_queue(), ^ {
                if (senderTag == 11) {
                    _repeatInterval = 0.05;
                    _timeLabel.text = [_setElements downTime];
                } else if (senderTag == 12) {
                    _repeatInterval = 0.05;
                    _timeLabel.text = [_setElements upTime];
                } else if (senderTag == 21) {
                    _repeatInterval = 0.002;
                    _distanceLabel.text = [_setElements downDistance];
                } else if (senderTag == 22) {
                    _repeatInterval = 0.002;
                    _distanceLabel.text = [_setElements upDistance];
                } else if (senderTag == 31) {
                    _repeatInterval = 0.02;
                    _countStepsLabel.text = [_setElements downCountSteps];
                } else if (senderTag == 32) {
                    _repeatInterval = 0.02;
                    _countStepsLabel.text = [_setElements upCountSteps];
                } else if (senderTag == 41) {
                    _repeatInterval = 0.02;
                    _calorieLabel.text = [_setElements downCalorie];
                } else if (senderTag == 42) {
                    _repeatInterval = 0.02;
                    _calorieLabel.text = [_setElements upCalorie];
                } else if (senderTag == 51) {
                    _repeatInterval = 0.05;
                    _heartRateLabel.text = [_setElements downHeartRate];
                } else if (senderTag == 52) {
                    _repeatInterval = 0.05;
                    _heartRateLabel.text = [_setElements upHeartRate];
                }
            });
            [self longPressChangeViewWithTag:senderTag];
        });
    }
}

- (void)setDefaultValues {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ () {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:SET_ELEMENTS_HAS_DEFAULT_VALUES];
        [[NSUserDefaults standardUserDefaults] setObject:_timeLabel.text forKey:SET_ELEMENTS_TIME];
        [[NSUserDefaults standardUserDefaults] setObject:_distanceLabel.text forKey:SET_ELEMENTS_DISTANCE];
        [[NSUserDefaults standardUserDefaults] setObject:_countStepsLabel.text forKey:SET_ELEMENTS_COUNT_STEPS];
        [[NSUserDefaults standardUserDefaults] setObject:_calorieLabel.text forKey:SET_ELEMENTS_CALORIE];
        [[NSUserDefaults standardUserDefaults] setObject:_heartRateLabel.text forKey:SET_ELEMENTS_HEART_RATE];
        [[NSUserDefaults standardUserDefaults] setInteger:_setModeChange.selectedSegmentIndex forKey:SET_ELEMENTS_SET_MODE];
    });
}

- (void)reviewDefaultValues {
    dispatch_async(dispatch_get_main_queue(), ^ {
        _timeLabel.text = _setElements.time;
        _distanceLabel.text = _setElements.distance;
        _countStepsLabel.text = _setElements.countSteps;
        _calorieLabel.text = _setElements.calorie;
        _heartRateLabel.text = _setElements.heartRate;
        _setModeChange.selectedSegmentIndex = _setElements.setMode;
        if (_setElements.setMode == SET_MODE_SPEED) {
            _distanceUnitLabel.text = [_setElements defaultDistanceUnit];
        }
    });
}

@end
