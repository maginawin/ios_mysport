//
//  WmMainTableViewController.h
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMSpeedElements.h"
#import "AppDelegate.h"

@interface WmMainTableViewController : UITableViewController

@property (weak, nonatomic) AppDelegate* appDelegate;

@end
