//
//  WMSetElements.m
//  MySport
//
//  Created by maginawin on 15/3/30.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import "WMSetElements.h"

#define NOTI_VALUE @"noti_value" //正常模式下的数据

@interface WMSetElements()

@end

@implementation WMSetElements

//@synthesize time = _time;
//@synthesize distance = _distance;
//@synthesize countSteps = _countSteps;
//@synthesize calorie = _calorie;
//@synthesize heartRate = _heartRate;
//@synthesize setMode = _setMode;

- (id)init {
    self = [super init];
    if (self) {
        _appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        BOOL hasDefaultValues = [[NSUserDefaults standardUserDefaults] boolForKey:SET_ELEMENTS_HAS_DEFAULT_VALUES];
        if (hasDefaultValues) {
            _time = [[NSUserDefaults standardUserDefaults] objectForKey:SET_ELEMENTS_TIME];
            _distance = [[NSUserDefaults standardUserDefaults] objectForKey:SET_ELEMENTS_DISTANCE];
            _countSteps = [[NSUserDefaults standardUserDefaults] objectForKey:SET_ELEMENTS_COUNT_STEPS];
            _calorie = [[NSUserDefaults standardUserDefaults] objectForKey:SET_ELEMENTS_CALORIE];
            _heartRate = [[NSUserDefaults standardUserDefaults] objectForKey:SET_ELEMENTS_HEART_RATE];
            _setMode = [[NSUserDefaults standardUserDefaults] integerForKey:SET_ELEMENTS_SET_MODE];
//            _distance = @"0.00";
//            _countSteps = @"0";
//            _calorie = @"0.0";
//            _heartRate = @"0";
//            _setMode = SET_MODE_SPEED;
        } else {
            _time = @"00:00";
            _distance = @"0.00";
            _countSteps = @"0";
            _calorie = @"0.0";
            _heartRate = @"0";
            _setMode = SET_MODE_SPEED;
        }
    }
    return self;
}

- (void)writeSetElementsToPeripheral {
    dispatch_queue_t writeQueue = dispatch_queue_create("WMSetElements.writeQueue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(writeQueue, ^ {
        if (_appDelegate.myPeripheral == nil || _appDelegate.myCentralManager.state != CBPeripheralStateConnected || !self) {
//            return;
        }
        [_appDelegate writeToPeripheral:[self setElemnetsForWrite]];
        NSLog(@" 1 : %@", [self setElemnetsForWrite]);
        // 心率另外发, 协议不同
        [NSThread sleepForTimeInterval:0.2];
        [_appDelegate writeToPeripheral:[self setHeartRateForWrite]];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_VALUE object:[self setHeartRateForWrite]];
        NSLog(@" 2 : %@", [self setHeartRateForWrite]);
    });
}

- (NSString*)setElemnetsForWrite {
    NSMutableString* result = [[NSMutableString alloc] init];
    if (_setMode == SET_MODE_SPEED) {
        // mode 1
        [result appendString:@"FD"];
        // _time 2
        NSString* hours = [_time substringWithRange:NSMakeRange(0, 2)];
        NSString* hexHours = [WWDTools hexStringFromString:hours];
        [result appendString:[NSString stringWithFormat:@"%@00", hexHours]];
        // KM 1
        [result appendString:@"00"];
        // dist 3
        NSMutableString* distanceMA = [[NSMutableString alloc] init];
        double distanceF = [_distance doubleValue];
        NSInteger distanceInteger = distanceF * 100;
        NSString* distanceS = [NSString stringWithFormat:@"%ld", (long int)distanceInteger];
        NSString* hexDistanceS = [WWDTools hexStringFromString:distanceS];
        if (hexDistanceS.length < 6) {
            for (int i = 0; i < 6 - hexDistanceS.length; i++) {
                [distanceMA appendString:@"0"];
            }
        }
        [distanceMA appendString:hexDistanceS];
        [result appendString:distanceMA];
        // cal 3
        NSMutableString* calMA = [[NSMutableString alloc] init];
        double calF = [_calorie doubleValue];
        NSInteger calInteger = calF * 10;
        NSString* calS = [NSString stringWithFormat:@"%ld", (long int)calInteger];
        NSString* hexCalS = [WWDTools hexStringFromString:calS];
        if (hexCalS.length < 6) {
            for (int i = 0; i < 6 - hexCalS.length; i++) {
                [calMA appendString:@"0"];
            }
        }
        [calMA appendString:hexCalS];
        [result appendString:calMA];
        // cnt 2
        NSMutableString* countStepsMA = [[NSMutableString alloc] init];
        NSInteger countSteps = [_countSteps integerValue];
        NSString* countStepsS = [NSString stringWithFormat:@"%d", (int)countSteps];
        NSString* hexCountStepsS = [WWDTools hexStringFromString:countStepsS];
        if (hexCountStepsS.length < 4) {
            for (int i = 0; i < 4 - hexCountStepsS.length; i++) {
                [countStepsMA appendString:@"0"];
            }
        }
        [countStepsMA appendString:hexCountStepsS];
        [result appendString:countStepsMA];
    } else if (_setMode == SET_MODE_PEDO) {
        [result appendString:@"FE"];
        // _time 2
        NSString* hours = [_time substringWithRange:NSMakeRange(0, 2)];
        NSString* hexHours = [WWDTools hexStringFromString:hours];
        [result appendString:[NSString stringWithFormat:@"%@00", hexHours]];
        // has dist 1
        [result appendString:@"01"];
        // cal 3
        NSMutableString* calMA = [[NSMutableString alloc] init];
        double calF = [_calorie doubleValue];
        NSInteger calInteger = calF * 10;
        NSString* calS = [NSString stringWithFormat:@"%ld", (long int)calInteger];
        NSString* hexCalS = [WWDTools hexStringFromString:calS];
        if (hexCalS.length < 6) {
            for (int i = 0; i < 6 - hexCalS.length; i++) {
                [calMA appendString:@"0"];
            }
        }
        [calMA appendString:hexCalS];
        [result appendString:calMA];
        // cnt 2
        NSMutableString* countStepsMA = [[NSMutableString alloc] init];
        NSInteger countSteps = [_countSteps integerValue];
        NSString* countStepsS = [NSString stringWithFormat:@"%d", (int)countSteps];
        NSString* hexCountStepsS = [WWDTools hexStringFromString:countStepsS];
        if (hexCountStepsS.length < 4) {
            for (int i = 0; i < 4 - hexCountStepsS.length; i++) {
                [countStepsMA appendString:@"0"];
            }
        }
        [countStepsMA appendString:hexCountStepsS];
        [result appendString:countStepsMA];
        // dist 3
        NSMutableString* distanceMA = [[NSMutableString alloc] init];
        double distanceF = [_distance doubleValue];
        NSInteger distanceInteger = distanceF * 100;
        NSString* distanceS = [NSString stringWithFormat:@"%ld", (long int)distanceInteger];
        NSString* hexDistanceS = [WWDTools hexStringFromString:distanceS];
        if (hexDistanceS.length < 6) {
            for (int i = 0; i < 6 - hexDistanceS.length; i++) {
                [distanceMA appendString:@"0"];
            }
        }
        [distanceMA appendString:hexDistanceS];
        [result appendString:distanceMA];
    }
    return result;
}

- (NSString*)setHeartRateForWrite {
    NSMutableString* resultMS = [[NSMutableString alloc] init];
    [resultMS appendString:@"F90000"];
    NSInteger heartRate = [_heartRate integerValue];
    NSString* heartRateS = [NSString stringWithFormat:@"%ld", (long int)heartRate];
    NSString* hexHeartRateS = [WWDTools hexStringFromString:heartRateS];
    if (hexHeartRateS.length < 4) {
        for (int i = 0; i < 4 - hexHeartRateS.length; i++) {
            [resultMS appendString:@"0"];
        }
    }
    [resultMS appendString:hexHeartRateS];
    return resultMS;
}

#pragma mark - Elements operation

- (NSString*)upTime {
    NSString* hours = [_time substringWithRange:NSMakeRange(0, 2)];
    NSInteger hoursInteger = [hours integerValue];
    if (hoursInteger == 99) {
        hoursInteger = 0;
    } else if (hoursInteger < 99) {
        hoursInteger += 1;
    }
    NSMutableString* hoursResult = [NSMutableString stringWithFormat:@"%d", (int)hoursInteger];
    if (hoursResult.length == 1) {
        [hoursResult insertString:@"0" atIndex:0];
    }
    NSMutableString* temp = [[NSMutableString alloc] initWithString:_time];
    [temp replaceCharactersInRange:NSMakeRange(0, 2) withString:hoursResult];
    _time = temp;
    return _time;
}

- (NSString*)downTime {
    NSString* hours = [_time substringWithRange:NSMakeRange(0, 2)];
    NSInteger hoursInteger = [hours integerValue];
    if (hoursInteger == 0) {
        hoursInteger = 99;
    } else if (hoursInteger > 0) {
        hoursInteger -= 1;
    }
    NSMutableString* hoursResult = [NSMutableString stringWithFormat:@"%d", (int)hoursInteger];
    if (hoursResult.length == 1) {
        [hoursResult insertString:@"0" atIndex:0];
    }
    NSMutableString* temp = [[NSMutableString alloc] initWithString:_time];
    [temp replaceCharactersInRange:NSMakeRange(0, 2) withString:hoursResult];
    _time = temp;
    return _time;
}

- (NSString*)upDistance {
    double distanceF = [_distance doubleValue];
    if (distanceF == 999.90) {
        distanceF = 0.00;
    } else if (distanceF < 999.90) {
        distanceF += 0.10;
    }
    _distance = [NSString stringWithFormat:@"%.2f", distanceF];
    return _distance;
}

- (NSString*)downDistance {
    double distanceF = [_distance doubleValue];
    if (distanceF == 0.00) {
        distanceF = 999.90;
    } else if (distanceF > 0) {
        distanceF -= 0.10;
    }
    _distance = [NSString stringWithFormat:@"%.2f", distanceF];
    return _distance;
}

- (NSString*)upCountSteps {
    NSInteger countStepsI = [_countSteps integerValue];
    if (countStepsI == 9990) {
        countStepsI = 0;
    } else if (countStepsI < 9990) {
        countStepsI += 10;
    }
    _countSteps = [NSString stringWithFormat:@"%d", (int)countStepsI];
    return _countSteps;
}

- (NSString*)downCountSteps {
    NSInteger countStepsI = [_countSteps integerValue];
    if (countStepsI == 0) {
        countStepsI = 9990;
    } else if (countStepsI > 0) {
        countStepsI -= 10;
    }
    _countSteps = [NSString stringWithFormat:@"%d", (int)countStepsI];
    return _countSteps;
}

- (NSString*)upCalorie {
    double calorieD = [_calorie doubleValue];
    if (calorieD == 999.0) {
        calorieD = 0.0;
    } else if (calorieD < 999.0) {
        calorieD += 1.0;
    }
    _calorie = [NSString stringWithFormat:@"%.1f", calorieD];
    return _calorie;
}

- (NSString*)downCalorie {
    double calorieD = [_calorie doubleValue];
    if (calorieD == 0.0) {
        calorieD = 999.0;
    } else if (calorieD > 0.0) {
        calorieD -= 1.0;
    }
    _calorie = [NSString stringWithFormat:@"%.1f", calorieD];
    return _calorie;
}

- (NSString*)upHeartRate {
    NSInteger heartRateI = [_heartRate integerValue];
    if (heartRateI == 0) {
        heartRateI = 40;
    } else if (heartRateI == 240) {
        heartRateI = 0;
    } else if (heartRateI >= 40) {
        heartRateI += 1;
    }
    _heartRate = [NSString stringWithFormat:@"%d", (int)heartRateI];
    return _heartRate;
}

- (NSString*)downHeartRate {
    NSInteger heartRateI = [_heartRate integerValue];
    if (heartRateI == 0) {
        heartRateI = 240;
    } else if (heartRateI == 40) {
        heartRateI = 0;
    } else if (heartRateI <= 240) {
        heartRateI -= 1;
    }
    _heartRate = [NSString stringWithFormat:@"%d", (int)heartRateI];
    return _heartRate;
}

- (void)updateDistanceUnit:(NSInteger)distanceUnit {
    _distanceUnit = distanceUnit;
    [[NSUserDefaults standardUserDefaults] setInteger:distanceUnit forKey:SET_MODE_DISTANCE_UNIT];
}

- (NSString*)defaultDistanceUnit {
    NSInteger result = [[NSUserDefaults standardUserDefaults] integerForKey:SET_MODE_DISTANCE_UNIT];
    if (result == 1) {
        return @"英里";
    }
    return @"千米";
}

@end
