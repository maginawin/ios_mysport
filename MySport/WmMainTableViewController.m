//
//  WmMainTableViewController.m
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import "WmMainTableViewController.h"

#define RADIAN_DEGREE(angle) ((angle) * M_PI / 180)

//NSNotification的NSString
#define NOTI_DIDCONNECTPERIPHERAL @"noti_didConnectPeripheral" //did connect peripheral
#define NOTI_DIDDISCONNECTPERIPHERAL @"noti_didDisconnetPeripheral" // did disconnect peripheral
#define NOTI_VALUE @"noti_value" //正常模式下的数据

@interface WmMainTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *connBleButton;
@property (weak, nonatomic) IBOutlet UIImageView *speedImageView;
@property (weak, nonatomic) IBOutlet UILabel *mUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrSetLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *calorieLabel;
@property (weak, nonatomic) IBOutlet UILabel *distLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalDistLabel;
@property (weak, nonatomic) IBOutlet UILabel *zhuansuLabel;
@property (weak, nonatomic) IBOutlet UILabel *chuanshuLabel;

@property (nonatomic) CGFloat currentSpeed;
@property (nonatomic) BOOL isRotating;
//@property (nonatomic) BOOL hasNewData;

@property (nonatomic, strong) WMSpeedElements* speedElements;

@end

@implementation WmMainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _currentSpeed = 0.0;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:36/255.0 green:190/255.0 blue:104/255.0 alpha:1];
    _isRotating = NO;
    _appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self updateSpeedImageViewSpeed:0];
    
    //notification
    NSNotificationCenter* notiCenter = [NSNotificationCenter defaultCenter];
    [notiCenter removeObserver:self];
    [notiCenter addObserver:self selector:@selector(notiDidConnectPeripheral:) name:NOTI_DIDCONNECTPERIPHERAL object:nil];
    [notiCenter addObserver:self selector:@selector(notiDidDisconnectPeripheral:) name:NOTI_DIDDISCONNECTPERIPHERAL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiValue:) name:NOTI_VALUE object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 给单位跟心率设定赋值
    dispatch_async(dispatch_get_main_queue(), ^ {
        _speedElements = [[WMSpeedElements alloc] init];
        _hrSetLabel.text = _speedElements.mHR;
//        _hasNewData = NO;
        _mUnitLabel.text = [NSString stringWithFormat:@"%@/时", _speedElements.mUnit];
        if (_appDelegate.myPeripheral.state == CBPeripheralStateConnected) {
            _connBleButton.image = [UIImage imageNamed:@"bleconnected"];
        } else {
            _connBleButton.image = [UIImage imageNamed:@"bleconnect"];
        }
//        [self resetViewsZero];
    });
}

#pragma mark - Notification

- (void)notiDidConnectPeripheral:(NSNotification*)sender {
    _connBleButton.image = [UIImage imageNamed:@"bleconnected"];
}

- (void)notiDidDisconnectPeripheral:(NSNotification*)sender {
    _connBleButton.image = [UIImage imageNamed:@"bleconnect"];
}

- (void)notiValue:(NSNotification*)sender {
//    _hasNewData = YES;
    NSString* value = [sender object];
    NSString* valueTag = [value substringWithRange:NSMakeRange(0, 2)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        // 正常模式
        if ([@"F7" isEqualToString:valueTag]) {
            // time 00:00
            NSString* minuteStr = [value substringWithRange:NSMakeRange(2, 2)];
            NSInteger minuteI = [WWDTools intFromHexString:minuteStr];
            if (minuteI < 10) {
                minuteStr = [NSString stringWithFormat:@"0%ld", (long int)minuteI];
            } else {
                minuteStr = [NSString stringWithFormat:@"%ld", (long int)minuteI];
            }
            NSString* secondStr = [value substringWithRange:NSMakeRange(4, 2)];
            NSInteger secondI = [WWDTools intFromHexString:secondStr];
            if (secondI < 10) {
                secondStr = [NSString stringWithFormat:@"0%ld", (long int)secondI];
            } else {
                secondStr = [NSString stringWithFormat:@"%ld", (long int)secondI];
            }
            dispatch_async(dispatch_get_main_queue(), ^ {
                _timeLabel.text = [NSString stringWithFormat:@"%@:%@", minuteStr, secondStr];
            });
            // unit 千米/时 或 英里/时
            NSString* kmStr = [value substringWithRange:NSMakeRange(6, 2)];
            if ([@"01" isEqualToString:kmStr]) {
                kmStr = @"英里";
            } else {
                kmStr = @"千米";
            }
            dispatch_async(dispatch_get_main_queue(), ^ {
                _mUnitLabel.text = [NSString stringWithFormat:@"%@/时", kmStr];
            });
            [[NSUserDefaults standardUserDefaults] setObject:kmStr forKey:SPEED_ELEMETNS_UNIT];
            // speed 0.0
            NSString* speedStr = [value substringWithRange:NSMakeRange(8, 4)];
            NSInteger speedI = [WWDTools intFromHexString:speedStr];
            float speedF = speedI / 10.00;
            [self updateSpeedImageViewSpeed:speedF];
            dispatch_async(dispatch_get_main_queue(), ^{
                _speedLabel.text = [NSString stringWithFormat:@"%.1f", speedF];
            });
            // distance 0.00
            NSString* distanceStr = [value substringWithRange:NSMakeRange(12, 6)];
            NSInteger distanceI = [WWDTools intFromHexString:distanceStr];
            float distanceF = distanceI / 100.000;
            dispatch_async(dispatch_get_main_queue(), ^ {
                _distLabel.text = [NSString stringWithFormat:@"%.2f", distanceF];
            });
            // calorie 0.0
            NSString* calorieS = [value substringWithRange:NSMakeRange(18, 6)];
            NSInteger calorieI = [WWDTools intFromHexString:calorieS];
            float calorieF = calorieI / 10.00;
            dispatch_async(dispatch_get_main_queue(), ^ {
                _calorieLabel.text = [NSString stringWithFormat:@"%.1f", calorieF];
            });
            // odo 0.0
            NSString* odoS = [value substringWithRange:NSMakeRange(24, 6)];
            int odoI = [WWDTools intFromHexString:odoS];
            float odoF = odoI / 10.00;
            dispatch_async(dispatch_get_main_queue(), ^{
                _totalDistLabel.text = [NSString stringWithFormat:@"%.1f", odoF];
            });
            // rpm
            NSString* rpmS = [value substringWithRange:NSMakeRange(30, 4)];
            int rpmI = [WWDTools intFromHexString:rpmS];
            dispatch_async(dispatch_get_main_queue(), ^ {
                _zhuansuLabel.text = [NSString stringWithFormat:@"%d", rpmI];
            });
            // cnt
            NSString* cntS = [value substringWithRange:NSMakeRange(34, 4)];
            int cntI = [WWDTools intFromHexString:cntS];
            dispatch_async(dispatch_get_main_queue(), ^ {
                _chuanshuLabel.text = [NSString stringWithFormat:@"%d", cntI];
            });
        } else if ([@"F9" isEqualToString:valueTag]) {
            // heart rate
            NSString* hrS = [value substringWithRange:NSMakeRange(2, 4)];
            int hrI = [WWDTools intFromHexString:hrS];
            NSString* hrSetS = [value substringWithRange:NSMakeRange(6, 4)];
            int hrSetI = [WWDTools intFromHexString:hrSetS];
            dispatch_async(dispatch_get_main_queue(), ^ {
                _hrLabel.text = [NSString stringWithFormat:@"%d", hrI];
                _hrSetLabel.text = [NSString stringWithFormat:@"%d", hrSetI];
            });
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", hrSetI] forKey:SPEED_ELEMENTS_HR];
        }
    });
}

// 清零
- (void)resetViewsZero {
//    if (!_hasNewData) {
        [self updateSpeedImageViewSpeed:0];
        _speedLabel.text = @"0.0";
        _hrLabel.text = @"0";
        _timeLabel.text = @"00:00";
        _calorieLabel.text = @"0.0";
        _distLabel.text = @"0.00";
        _totalDistLabel.text = @"0.0";
        _zhuansuLabel.text = @"0";
        _chuanshuLabel.text = @"0";
//    }
}

// 传入速度, 改表盘指针位置
- (void)updateSpeedImageViewSpeed:(CGFloat)speed {
    __block CGFloat speed0 = speed;
    dispatch_queue_t speedQueue = dispatch_queue_create("speedQueue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(speedQueue, ^ {
        if (speed0 > 80) {
            speed0 = 80;
        } else if (speed0 < 0) {
            speed0 = 0;
        }
        if (!_isRotating) {
            _isRotating = YES;
            CGFloat speedAngle = [self angleFromSpeed:speed0];
            CGFloat diffSpeed = fabs(speed0 - _currentSpeed);
            _currentSpeed = speed0;
            dispatch_async(dispatch_get_main_queue(), ^ {
                if (diffSpeed < 60) {
                    CGAffineTransform speedTransform = CGAffineTransformMakeRotation(speedAngle);
                    [UIView animateWithDuration:0.2 animations:^ {
                        _speedImageView.transform = speedTransform;
                    } completion:^ (BOOL finished) {
                        if (finished) {
                            _isRotating = NO;
                        }
                    }];
                } else {
                    [UIView animateWithDuration:0.1 animations:^ {
                        _speedImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
                    } completion:^ (BOOL finished) {
                        [UIView animateWithDuration:0.1 animations:^ {
                            _speedImageView.transform = CGAffineTransformMakeRotation(speedAngle);
                        } completion:^ (BOOL finished) {
                            if (finished) {
                                _isRotating = NO;
                            }
                        }];
                    }];
                }
                _isRotating = NO;
            });
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.11 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self updateSpeedImageViewSpeed:speed];
            });
        }
        // NSLog(@"speed %lf, current %lf", speed0, _currentSpeed);
    });
    
}

- (CGFloat)angleFromSpeed:(CGFloat)speed {
    if (speed < 10) {
        return RADIAN_DEGREE(speed * 3 - 30);
    }
    return RADIAN_DEGREE(3 * (speed - 10));
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

#pragma mark - Test method

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
