//
//  CDViewController.m
//  MySport
//
//  Created by maginawin on 14-11-20.
//  Copyright (c) 2014年 mycj.wwd. All rights reserved.
//

#import "CDViewController.h"
#import "WWDTools.h"

#define NOTI_VALUE @"noti_value" //正常模式下的数据

@interface CDViewController ()

@end

@implementation CDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiValue:) name:NOTI_VALUE object:nil];
}

- (void)notiValue:(NSNotification*)nofity{
    NSString* value = [nofity object];
    NSString* idString = [WWDTools stringFromIndexCount:0 count:2 from:value]; //取得value的前两位字符
    //倒计
    if ([@"FD" isEqual:idString]) {
        //time
        NSString* minuteStr = @"";
        NSString* secondStr = @"";
        NSString* minuteHexStr = [WWDTools stringFromIndexCount:2 count:2 from:value];
        NSString* secondHexStr = [WWDTools stringFromIndexCount:4 count:2 from:value];
        NSUInteger minuteInt = [WWDTools intFromHexString:minuteHexStr];
        NSUInteger secondInt = [WWDTools intFromHexString:secondHexStr];
        if (minuteInt < 10) {
            minuteStr = [NSString stringWithFormat:@"0%lu", (unsigned long)minuteInt];
        }else{
            minuteStr = [NSString stringWithFormat:@"%lu", (unsigned long)minuteInt];
        }
        if (secondInt < 10) {
            secondStr = [NSString stringWithFormat:@"0%lu", (unsigned long)secondInt];
        }else{
            secondStr = [NSString stringWithFormat:@"%lu", (unsigned long)secondInt];
        }
        _timeLabel.text = [NSString stringWithFormat:@"%@:%@", minuteStr, secondStr];
        
        //dan wei
        NSString* modeStr = [WWDTools stringFromIndexCount:6 count:2 from:value];
        if ([@"01" isEqual:modeStr]) {
            NSString* mode = @"英里";
            _distDanWeiLabel.text = mode;
            _cntDanWeiLabel.text = mode;
        }else{
            NSString* mode = @"公里";
            _distDanWeiLabel.text = mode;
            _cntDanWeiLabel.text = mode;
        }
        
        //dist 0.00
        NSString* distHexStr = [WWDTools stringFromIndexCount:8 count:6 from:value];
        NSUInteger distInt = [WWDTools intFromHexString:distHexStr];
        float distF = distInt / 100.0;
        _distanceLabel.text = [NSString stringWithFormat:@"%0.2f", distF];
        
        //calorie 0.0
        NSString* calorieHexStr = [WWDTools stringFromIndexCount:14 count:6 from:value];
        NSUInteger calorieInt = [WWDTools intFromHexString:calorieHexStr];
        float calorieF = calorieInt / 10.0;
        _calorieLabel.text = [NSString stringWithFormat:@"%0.1f", calorieF];
        
        //cnt
        NSString* cntHexStr = [WWDTools stringFromIndexCount:20 count:4 from:value];
        NSUInteger cntInt = [WWDTools intFromHexString:cntHexStr];
        _cntDanWeiLabel.text = [NSString stringWithFormat:@"%u", (unsigned int)cntInt];
        
    }
    //心率
    else if([@"F9" isEqual:idString]){
        NSString* hrHexStr = [WWDTools stringFromIndexCount:2 count:4 from:value];
        NSUInteger hrInt = [WWDTools intFromHexString:hrHexStr];
        _heartRateLabel.text = [NSString stringWithFormat:@"%u", (unsigned int)hrInt];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTI_VALUE object:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
