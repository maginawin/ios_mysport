//
//  WMPedoElements.h
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "WWDTools.h"

#define PEDO_ELEMENTS_HR @"pedolementsHR"
#define PEDO_ELEMENTS_TIME @"pedoElemetsTime"
#define PEDO_ELEMENTS_DISTANCE @"pedoElementsDistance"
#define PEDO_ELEMENTS_STEPS @"pedoElementsStes"
#define PEDO_ELEMENTS_CALORIE @"pedoElementsCalorie"
#define PEDO_ELEMENTS_HAS_SAVED @"pedoElementsHasSaved"
#define PEDO_ELEMETNS_UNIT @"pedoElementsUnit"

@interface WMPedoElements : NSObject

@property (nonatomic, strong) NSString* mTime;
@property (nonatomic, strong) NSString* mDist;
@property (nonatomic, strong) NSString* mSteps;
@property (nonatomic, strong) NSString* mCalorie;
@property (nonatomic, strong) NSString* mHR;
@property (nonatomic, strong) NSString* mUnit;

@property (weak, nonatomic) AppDelegate* appDelegate;

- (void)writeElementsToPeripheral;

- (NSString*)upTime;
- (NSString*)downTime;
- (NSString*)upDist;
- (NSString*)downDist;
- (NSString*)upSteps;
- (NSString*)downSteps;
- (NSString*)upCalorie;
- (NSString*)downCalorie;
- (NSString*)upHR;
- (NSString*)downHR;

@end
