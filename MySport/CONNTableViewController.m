//
//  CONNTableViewController.m
//  MySport
//
//  Created by maginawin on 14-11-24.
//  Copyright (c) 2014年 mycj.wwd. All rights reserved.
//

#import "CONNTableViewController.h"

//NSNotification的NSString
#define NOTI_DIDDISCOVERPERIPHERAL @"noti_didDiscoverPeripheral" //发现peripheral

@interface CONNTableViewController ()
@property (nonatomic, strong) UIRefreshControl* refreshA;
@end

@implementation CONNTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _myAppDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    //下拉刷新
    
    _refreshA = [[UIRefreshControl alloc] init];
    _refreshA.tintColor = [UIColor whiteColor];
    _refreshA.attributedTitle = [[NSMutableAttributedString alloc]initWithString:@"下拉刷新" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
//    _refreshA.attributedTitle = [[NSAttributedString alloc] initWithString:@"下拉刷新"];
    [_refreshA addTarget:self action:@selector(refreshBle) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshA];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    //notification
    NSNotificationCenter* notiCenter = [NSNotificationCenter defaultCenter];
    [notiCenter removeObserver:self];
    [notiCenter addObserver:self selector:@selector(notiDidDiscoverPeripheral:) name:NOTI_DIDDISCOVERPERIPHERAL object:nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)refreshBle{
//    _refreshA.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"refreshing", @"")];
//    _refreshA.attributedTitle = [[NSAttributedString alloc] initWithString:@"正在刷新..."];
    _refreshA.attributedTitle = [[NSMutableAttributedString alloc]initWithString:@"正在刷新..." attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self scanBle];
    [self performSelector:@selector(handleBle) withObject:nil afterDelay:1];
}

- (void)handleBle{
    [_refreshA endRefreshing];
//    _refreshA.attributedTitle = [[NSAttributedString alloc] initWithString:@"下拉刷新"];
    _refreshA.attributedTitle = [[NSMutableAttributedString alloc]initWithString:@"下拉刷新" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    //tableview reload
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self scanBle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_myAppDelegate.myPeripherals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* cellId = @"cellID";
    NSUInteger rowNo = indexPath.row;
    //get cell style as bleCell
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    //get bleName & bleAddress label
    UILabel* bleName = (UILabel*)[cell viewWithTag:1];
    UILabel* bleAddress = (UILabel*)[cell viewWithTag:2];
    //get name & uuid from myPeripherals array
    NSString* name = [NSString stringWithFormat:@"%@",[[_myAppDelegate.myPeripherals objectAtIndex:rowNo] name]];
    NSString* uuid = [NSString stringWithFormat:@"%@",[[_myAppDelegate.myPeripherals objectAtIndex:rowNo] identifier]];
    uuid = [uuid substringFromIndex:[uuid length] - 12 - 24];
    //assign bleName & bleAddress
    bleName.text = name;
    bleAddress.text = uuid;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //让tableview的cell选中状态逐渐消失
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_myAppDelegate.myPeripheral != nil) {
        [_myAppDelegate.myCentralManager cancelPeripheralConnection:_myAppDelegate.myPeripheral];
        _myAppDelegate.myPeripheral = nil;
    }
    
    NSUInteger rowNo = indexPath.row; //行号
    _myAppDelegate.myPeripheral = [_myAppDelegate.myPeripherals objectAtIndex:rowNo]; //根据行号给myPeripheral赋值
    [_myAppDelegate connectClick]; //连接已经赋值的myPeripheral
    [self.navigationController popViewControllerAnimated:YES]; //选择连接后自动跳出connect view
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)scanBle{
    //重新查找peripherals
    [_myAppDelegate scanClick];
    [self.tableView reloadData];
}

- (void)notiDidDiscoverPeripheral:(NSNotification*)notify{
    [self.tableView reloadData];
}

@end
