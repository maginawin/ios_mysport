//
//  WMSpeedElements.m
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import "WMSpeedElements.h"

@implementation WMSpeedElements

- (id)init {
    self = [super init];
    if (self) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:SPEED_ELEMENTS_HAS_SAVED]) {
            _mTime = [[NSUserDefaults standardUserDefaults] objectForKey:SPEED_ELEMENTS_TIME];
            _mDist = [[NSUserDefaults standardUserDefaults] objectForKey:SPEED_ELEMENTS_DISTANCE];
            _mSteps = [[NSUserDefaults standardUserDefaults] objectForKey:SPEED_ELEMENTS_STEPS];
            _mCalorie = [[NSUserDefaults standardUserDefaults] objectForKey:SPEED_ELEMENTS_CALORIE];
            _mHR = [[NSUserDefaults standardUserDefaults] objectForKey:SPEED_ELEMENTS_HR];
            _mUnit = [[NSUserDefaults standardUserDefaults] objectForKey:SPEED_ELEMETNS_UNIT];
        } else {
            [self setupDefaultElements];
        }
        _appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    }
    return self;
}

- (void)writeElementsToPeripheral {
//    dispatch_queue_t writeQueue = dispatch_queue_create("WMSetElements.writeQueue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
//        if (_appDelegate.myPeripheral == nil || _appDelegate.myCentralManager.state != CBPeripheralStateConnected || !self) {
//            //            return;
//        }
        [_appDelegate writeToPeripheral:[self elementsForWrite]];
        NSLog(@" 1 : %@", [self elementsForWrite]);
        // 心率另外发, 协议不同
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_appDelegate writeToPeripheral:[self hrForWrite]];
        });
        NSLog(@" 2 : %@", [self hrForWrite]);
        // 将新的值保存入本地
        [self setupNewElements];
    });

}

- (NSString*)elementsForWrite {
    NSMutableString* result = [[NSMutableString alloc] init];
    [result appendString:@"FD"];
    // _time 2
    NSString* hours = [_mTime substringWithRange:NSMakeRange(0, 2)];
    NSString* hexHours = [WWDTools hexStringFromString:hours];
    [result appendString:[NSString stringWithFormat:@"%@00", hexHours]];
    // KM 1
    [result appendString:@"00"];
    // dist 3
    NSMutableString* distanceMA = [[NSMutableString alloc] init];
    double distanceF = [_mDist doubleValue];
    NSInteger distanceInteger = distanceF * 100;
    NSString* distanceS = [NSString stringWithFormat:@"%ld", (long int)distanceInteger];
    NSString* hexDistanceS = [WWDTools hexStringFromString:distanceS];
    if (hexDistanceS.length < 6) {
        for (int i = 0; i < 6 - hexDistanceS.length; i++) {
            [distanceMA appendString:@"0"];
        }
    }
    [distanceMA appendString:hexDistanceS];
    [result appendString:distanceMA];
    // cal 3
    NSMutableString* calMA = [[NSMutableString alloc] init];
    double calF = [_mCalorie doubleValue];
    NSInteger calInteger = calF * 10;
    NSString* calS = [NSString stringWithFormat:@"%ld", (long int)calInteger];
    NSString* hexCalS = [WWDTools hexStringFromString:calS];
    if (hexCalS.length < 6) {
        for (int i = 0; i < 6 - hexCalS.length; i++) {
            [calMA appendString:@"0"];
        }
    }
    [calMA appendString:hexCalS];
    [result appendString:calMA];
    // cnt 2
    NSMutableString* countStepsMA = [[NSMutableString alloc] init];
    NSInteger countSteps = [_mSteps integerValue];
    NSString* countStepsS = [NSString stringWithFormat:@"%d", (int)countSteps];
    NSString* hexCountStepsS = [WWDTools hexStringFromString:countStepsS];
    if (hexCountStepsS.length < 4) {
        for (int i = 0; i < 4 - hexCountStepsS.length; i++) {
            [countStepsMA appendString:@"0"];
        }
    }
    [countStepsMA appendString:hexCountStepsS];
    [result appendString:countStepsMA];
    return result;
}

- (NSString*)hrForWrite {
    NSMutableString* resultMS = [[NSMutableString alloc] init];
    [resultMS appendString:@"F90000"];
    NSInteger heartRate = [_mHR integerValue];
    NSString* heartRateS = [NSString stringWithFormat:@"%ld", (long int)heartRate];
    NSString* hexHeartRateS = [WWDTools hexStringFromString:heartRateS];
    if (hexHeartRateS.length < 4) {
        for (int i = 0; i < 4 - hexHeartRateS.length; i++) {
            [resultMS appendString:@"0"];
        }
    }
    [resultMS appendString:hexHeartRateS];
    return resultMS;
}

- (void)setupDefaultElements {
    _mTime = @"00:00";
    _mDist = @"0.00";
    _mSteps = @"0";
    _mCalorie = @"0.0";
    _mHR = @"0";
    _mUnit = @"千米";
}

- (void)setupNewElements {
    [[NSUserDefaults standardUserDefaults] setObject:_mTime forKey:SPEED_ELEMENTS_TIME];
    [[NSUserDefaults standardUserDefaults] setObject:_mDist forKey:SPEED_ELEMENTS_DISTANCE];
    [[NSUserDefaults standardUserDefaults] setObject:_mSteps forKey:SPEED_ELEMENTS_STEPS];
    [[NSUserDefaults standardUserDefaults] setObject:_mCalorie forKey:SPEED_ELEMENTS_CALORIE];
    [[NSUserDefaults standardUserDefaults] setObject:_mHR forKey:SPEED_ELEMENTS_HR];
    [[NSUserDefaults standardUserDefaults] setObject:_mUnit forKey:SPEED_ELEMETNS_UNIT];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:SPEED_ELEMENTS_HAS_SAVED];
}

#pragma mark - UP & DOWN 

- (NSString*)upTime {
    NSString* hours = [_mTime substringWithRange:NSMakeRange(0, 2)];
    NSInteger hoursInteger = [hours integerValue];
    if (hoursInteger == 99) {
        hoursInteger = 0;
    } else if (hoursInteger < 99) {
        hoursInteger += 1;
    }
    NSMutableString* hoursResult = [NSMutableString stringWithFormat:@"%d", (int)hoursInteger];
    if (hoursResult.length == 1) {
        [hoursResult insertString:@"0" atIndex:0];
    }
    NSMutableString* temp = [[NSMutableString alloc] initWithString:_mTime];
    [temp replaceCharactersInRange:NSMakeRange(0, 2) withString:hoursResult];
    _mTime = temp;
    return _mTime;
}

- (NSString*)downTime {
    NSString* hours = [_mTime substringWithRange:NSMakeRange(0, 2)];
    NSInteger hoursInteger = [hours integerValue];
    if (hoursInteger == 0) {
        hoursInteger = 99;
    } else if (hoursInteger > 0) {
        hoursInteger -= 1;
    }
    NSMutableString* hoursResult = [NSMutableString stringWithFormat:@"%d", (int)hoursInteger];
    if (hoursResult.length == 1) {
        [hoursResult insertString:@"0" atIndex:0];
    }
    NSMutableString* temp = [[NSMutableString alloc] initWithString:_mTime];
    [temp replaceCharactersInRange:NSMakeRange(0, 2) withString:hoursResult];
    _mTime = temp;
    return _mTime;
}

- (NSString*)upDist {
    double distanceF = [_mDist doubleValue];
    if (distanceF == 999.90) {
        distanceF = 0.00;
    } else if (distanceF < 999.90) {
        distanceF += 0.10;
    }
    _mDist = [NSString stringWithFormat:@"%.2f", distanceF];
    return _mDist;
}

- (NSString*)downDist {
    double distanceF = [_mDist doubleValue];
    if (distanceF == 0.00) {
        distanceF = 999.90;
    } else if (distanceF > 0) {
        distanceF -= 0.10;
    }
    _mDist = [NSString stringWithFormat:@"%.2f", distanceF];
    return _mDist;
}

- (NSString*)upSteps {
    NSInteger countStepsI = [_mSteps integerValue];
    if (countStepsI == 9990) {
        countStepsI = 0;
    } else if (countStepsI < 9990) {
        countStepsI += 10;
    }
    _mSteps = [NSString stringWithFormat:@"%d", (int)countStepsI];
    return _mSteps;
}

- (NSString*)downSteps {
    NSInteger countStepsI = [_mSteps integerValue];
    if (countStepsI == 0) {
        countStepsI = 9990;
    } else if (countStepsI > 0) {
        countStepsI -= 10;
    }
    _mSteps = [NSString stringWithFormat:@"%d", (int)countStepsI];
    return _mSteps;
}

- (NSString*)upCalorie {
    double calorieD = [_mCalorie doubleValue];
    if (calorieD == 999.0) {
        calorieD = 0.0;
    } else if (calorieD < 999.0) {
        calorieD += 1.0;
    }
    _mCalorie = [NSString stringWithFormat:@"%.1f", calorieD];
    return _mCalorie;
}

- (NSString*)downCalorie {
    double calorieD = [_mCalorie doubleValue];
    if (calorieD == 0.0) {
        calorieD = 999.0;
    } else if (calorieD > 0.0) {
        calorieD -= 1.0;
    }
    _mCalorie = [NSString stringWithFormat:@"%.1f", calorieD];
    return _mCalorie;
}

- (NSString*)upHR {
    NSInteger heartRateI = [_mHR integerValue];
    if (heartRateI == 0) {
        heartRateI = 40;
    } else if (heartRateI == 240) {
        heartRateI = 0;
    } else if (heartRateI >= 40) {
        heartRateI += 1;
    }
    _mHR = [NSString stringWithFormat:@"%d", (int)heartRateI];
    return _mHR;
}

- (NSString*)downHR {
    NSInteger heartRateI = [_mHR integerValue];
    if (heartRateI == 0) {
        heartRateI = 240;
    } else if (heartRateI == 40) {
        heartRateI = 0;
    } else if (heartRateI <= 240) {
        heartRateI -= 1;
    }
    _mHR = [NSString stringWithFormat:@"%d", (int)heartRateI];
    return _mHR;
}


@end
