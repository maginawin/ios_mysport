//
//  CONNTableViewController.h
//  MySport
//
//  Created by maginawin on 14-11-24.
//  Copyright (c) 2014年 mycj.wwd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CONNTableViewController : UITableViewController

@property (weak, nonatomic) AppDelegate* myAppDelegate;

@end
