//
//  AppDelegate.m
//  MySport
//
//  Created by maginawin on 14-11-20.
//  Copyright (c) 2014年 mycj.wwd. All rights reserved.
//

#import "AppDelegate.h"
#import "WWDTools.h"

//NSNotification的NSString
#define NOTI_DIDDISCOVERPERIPHERAL @"noti_didDiscoverPeripheral" //did discover peripheral
#define NOTI_DIDCONNECTPERIPHERAL @"noti_didConnectPeripheral" //did connect peripheral
#define NOTI_DIDDISCONNECTPERIPHERAL @"noti_didDisconnetPeripheral" // id disconnect peripheral
#define NOTI_VALUE @"noti_value" //正常模式下的数据

//UUID
#define UUID_READ @"FFF1" // read characteristic
#define UUID_WRITE @"FFF2" // write characteristic



@interface AppDelegate ()
@property (nonatomic, strong) NSNotificationCenter* notiCenter;
@property (nonatomic, strong) NSTimer* timer;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    _notiCenter = [NSNotificationCenter defaultCenter];
    _bleConnectStatus = NO;
    
    //初始化蓝牙centralManager
    _myCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    _myPeripherals = [NSMutableArray array]; //初始化peripherals,用来存储所有的remote device

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - CoreBluetooth

//开始查看服务,蓝牙是否开启
- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    switch (central.state) {
        case CBCentralManagerStatePoweredOff:
            //蓝牙关闭
            [_notiCenter postNotificationName:NOTI_DIDDISCONNECTPERIPHERAL object:nil];
            _bleConnectStatus = NO;
            break;
        case CBCentralManagerStatePoweredOn:
            //蓝牙开启
            break;
        default:
            break;
    }
}

//查找到外设后的方法
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    //    // NSLog(@"peripheral : %@",peripheral.name);
//    if ([MYCJ_BLE_NAME isEqualToString:peripheral.name]) {
        [_myPeripherals addObject:peripheral]; //将发现的peripheral添加到myPeripherals中去
        [_notiCenter postNotificationName:NOTI_DIDDISCOVERPERIPHERAL object:nil]; //发送发现外围的通知
//    }
    //    // NSLog(@"查找到外设: %@,\n RSSI: %@,\n UUID: %@\n", peripheral, RSSI, [peripheral identifier]);
}

//连接外围成功
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    [peripheral setDelegate:self]; //设置myPeripheral的delegate为self
    [peripheral discoverServices:nil]; //查找services
    [_notiCenter postNotificationName:NOTI_DIDCONNECTPERIPHERAL object:nil]; //发送连接外围成功的通知
    _bleConnectStatus = YES;
    _myPeripheral = peripheral;
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

//连接掉线
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    if(!_myPeripheral){
        return;
    }
    [_notiCenter postNotificationName:NOTI_DIDDISCONNECTPERIPHERAL object:nil];
    _bleConnectStatus = NO;
    [_myCentralManager connectPeripheral:peripheral options:nil]; //掉线重连
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    //    // NSLog(@"掉线");
}

//已发现服务
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    int i = 0;
    for (CBService* s in peripheral.services){
        i++;
        [peripheral discoverCharacteristics:nil forService:s];
    }
}

//已发现characteristic
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    for (CBCharacteristic* c in service.characteristics) {
        CBUUID* cUUID = c.UUID;
        if ([cUUID isEqual:[CBUUID UUIDWithString:UUID_WRITE]]) {
            _writeCharacteristic = c;
            [self writeToPeripheral:[WWDTools getNowTimeToNSStringFromWrite]]; //同步时间发送去remote
        }else if([cUUID isEqual:[CBUUID UUIDWithString:UUID_READ]]){
            _readCharacteristic = c;
            [peripheral setNotifyValue:YES forCharacteristic:c];
        }
    }
}

//readRSSI的回调函数
- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error{
    
//    if ([RSSI intValue] <= -94) {
//        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//        BOOL disconnectedAlarmValue = [defaults boolForKey:DEFAULTS_ALALARM];
//        if (!disconnectedAlarmValue) {
//            [self avAudioPlayerStartOnceFromWAV:@"searchiPhoneAlarm2"];
//        }
//    }
    //    // NSLog(@"rssi read %d", [RSSI intValue]);
}

//中心读取外设实时数据
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
}

//获取外设发来的数据,不论是read和notify,获取数据都从这个方法
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSData* data = characteristic.value; //获得data
    NSString* value = [[WWDTools hexadecimalString:data] uppercaseString]; //将data转为NSString并转换为大写
//    NSString* idString = [WWDTools stringFromIndexCount:0 count:2 from:value]; //取得value的前两位字符作为id
    // NSLog(@"value: %@", value);
    [_notiCenter postNotificationName:NOTI_VALUE object:value];
}

//查找remote device
- (void)scanClick{
    if (_myPeripherals != nil) {
        _myPeripherals = nil;
        _myPeripherals = [NSMutableArray array];
    }
    
    [_myCentralManager scanForPeripheralsWithServices:nil options:nil];
    
    //5秒后自动关闭扫描
    double delayInSeconds = 5.0f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds* NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.myCentralManager stopScan];
        //        // NSLog(@"扫描超时,停止扫描!");
    });
}

//连接myPeripheral
- (void)connectClick{
    [_myCentralManager connectPeripheral:_myPeripheral options:nil];
}

//向myPeripheral中的writeCharacteristic中写入数据
- (void)writeToPeripheral:(NSString *)data{
    if (!_writeCharacteristic || !_myPeripheral) {
        return;
    }
    
    NSData* value = [WWDTools dataWithHexstring:data];
    [_myPeripheral writeValue:value forCharacteristic:_writeCharacteristic type:CBCharacteristicWriteWithResponse];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "mycj.wwd.MySport" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MySport" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MySport.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
