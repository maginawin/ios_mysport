//
//  ViewController.h
//  MySport
//
//  Created by maginawin on 14-11-20.
//  Copyright (c) 2014年 mycj.wwd. All rights reserved.
//  test bithucket net

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "WMSpeedElements.h"

//#define SPEED_ELEMENTS_HR @"speedElementsHR"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *zhenImage;
@property (weak, nonatomic) IBOutlet UITableView *mysportTableView;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedDanWeiLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *connBtn;
@property (weak, nonatomic) AppDelegate* myAppDelegate;

//根据传入的imageView及角度来旋转imageView
- (void)rotateImageView:(UIImageView*)imageView withAngle:(CGFloat)angle;

@end

