//
//  WMSpeedTableViewController.h
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMSpeedElements.h"

@interface WMSpeedTableViewController : UITableViewController

@property (nonatomic, strong) WMSpeedElements* speedElements;

@end
