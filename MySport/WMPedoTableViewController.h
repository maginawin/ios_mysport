//
//  WMPedoTableViewController.h
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMPedoElements.h"

@interface WMPedoTableViewController : UITableViewController

@property (nonatomic, strong) WMPedoElements* setElements;

@end
