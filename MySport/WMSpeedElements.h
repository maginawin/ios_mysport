//
//  WMSpeedElements.h
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "WWDTools.h"

#define SPEED_ELEMENTS_HR @"speedElementsHR"
#define SPEED_ELEMENTS_TIME @"speedElemetsTime"
#define SPEED_ELEMENTS_DISTANCE @"speedElementsDistance"
#define SPEED_ELEMENTS_STEPS @"speedElementsStes"
#define SPEED_ELEMENTS_CALORIE @"speedElementsCalorie"
#define SPEED_ELEMENTS_HAS_SAVED @"speedElementsHasSaved"
#define SPEED_ELEMETNS_UNIT @"speedElementsUnit"

@interface WMSpeedElements : NSObject

@property (nonatomic, strong) NSString* mTime;
@property (nonatomic, strong) NSString* mDist;
@property (nonatomic, strong) NSString* mSteps;
@property (nonatomic, strong) NSString* mCalorie;
@property (nonatomic, strong) NSString* mHR;
@property (nonatomic, strong) NSString* mUnit;

@property (weak, nonatomic) AppDelegate* appDelegate;

- (void)writeElementsToPeripheral;

- (NSString*)upTime;
- (NSString*)downTime;
- (NSString*)upDist;
- (NSString*)downDist;
- (NSString*)upSteps;
- (NSString*)downSteps;
- (NSString*)upCalorie;
- (NSString*)downCalorie;
- (NSString*)upHR;
- (NSString*)downHR;

@end
