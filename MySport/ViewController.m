//
//  ViewController.m
//  MySport
//
//  Created by maginawin on 14-11-20.
//  Copyright (c) 2014年 mycj.wwd. All rights reserved.
//

#import "ViewController.h"
#import "WWDTools.h"

//NSNotification的NSString
#define NOTI_DIDCONNECTPERIPHERAL @"noti_didConnectPeripheral" //did connect peripheral
#define NOTI_DIDDISCONNECTPERIPHERAL @"noti_didDisconnetPeripheral" // did disconnect peripheral
#define NOTI_VALUE @"noti_value" //正常模式下的数据

#define RADIAN_DEGREE(angle) (angle * M_PI / 180)

@interface ViewController ()

@property (nonatomic, strong) NSString* speedStr;
@property (nonatomic, strong) NSString* speedDanWeiStr;
@property (nonatomic, strong) NSString* heartRateStr;
@property (nonatomic, strong) NSString* heartRateXianStr;
@property (nonatomic, strong) NSString* timeStr;
@property (nonatomic, strong) NSString* calorieStr;
@property (nonatomic, strong) NSString* distanceStr;
@property (nonatomic, strong) NSString* odoStr;
@property (nonatomic, strong) NSString* rpmStr;
@property (nonatomic, strong) NSString* cntStr;

@property (nonatomic) float bSpeedF;

@property (nonatomic, strong) UIImage* connBleImage;
@property (nonatomic, strong) UIImage* connectedBleImage;

@property (nonatomic, strong) UILabel* mHRSetLabel;

@end

@implementation ViewController
CGFloat testV;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _myAppDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view, typically from a nib.
    UIColor* lightGreenColor = [UIColor colorWithRed:36.0/255 green:190.0/255 blue:104.0/255 alpha:1.0];
    //将tabbar的item字体颜色改为红色
    [self.tabBarController.tabBar setTintColor:lightGreenColor];
    [self rotateImageView:_zhenImage withAngle:-30];
    _mysportTableView.delegate = self;
    _mysportTableView.dataSource = self;
    _mysportTableView.tableFooterView = [[UIView alloc] init];
    
    _speedStr = @"0.0";
    _speedDanWeiStr = @"KM/H";
    _heartRateStr = @"0";
    _heartRateXianStr = @"0";
    _timeStr = @"00:00";
    _calorieStr = @"0";
    _distanceStr = @"0.00";
    _odoStr = @"0.0";
    _rpmStr = @"0";
    _cntStr = @"0";
    _bSpeedF = 0.0f;
    _speedLabel.text = _speedStr;
    _speedDanWeiLabel.text = _speedDanWeiStr;
    
    _connBleImage = [UIImage imageNamed:@"bleconnect"];
    _connectedBleImage = [UIImage imageNamed:@"bleconnected"];
    
    //notification
    NSNotificationCenter* notiCenter = [NSNotificationCenter defaultCenter];
    [notiCenter removeObserver:self];
    [notiCenter addObserver:self selector:@selector(notiDidConnectPeripheral:) name:NOTI_DIDCONNECTPERIPHERAL object:nil];
    [notiCenter addObserver:self selector:@selector(notiDidDisconnectPeripheral:) name:NOTI_DIDDISCONNECTPERIPHERAL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiValue:) name:NOTI_VALUE object:nil];
    
    _zhenImage.layer.anchorPoint = CGPointMake(0.5, 0.5);
}

- (void)notiDidConnectPeripheral:(NSNotification*)notify{
    [_connBtn setImage:_connectedBleImage];
    
    //    NSLog(@"did connect");
}

- (void)notiDidDisconnectPeripheral:(NSNotification*)notify{
    [_connBtn setImage:_connBleImage];
    //    NSLog(@"did disconnect");
}

- (void)notiValue:(NSNotification*)notify{
    NSString *value = [notify object];
    NSString* idString = [WWDTools stringFromIndexCount:0 count:2 from:value]; //取得value的前两位字符
    //正常
    if ([@"F7" isEqual:idString]) {
        //分
        NSString* minuteStr = [WWDTools stringFromIndexCount:2 count:2 from:value];
        NSUInteger minuteInt = [WWDTools intFromHexString:minuteStr];
        if (minuteInt < 10) {
            minuteStr = [NSString stringWithFormat:@"0%lu", (unsigned long)minuteInt];
        }else{
            minuteStr = [NSString stringWithFormat:@"%lu", (unsigned long)minuteInt];
        }
        //秒
        NSString* secondStr = [WWDTools stringFromIndexCount:4 count:2 from:value];
        NSUInteger secondInt = [WWDTools intFromHexString:secondStr];
        if (secondInt < 10) {
            secondStr = [NSString stringWithFormat:@"0%lu", (unsigned long)secondInt];
        }else{
            secondStr = [NSString stringWithFormat:@"%lu", (unsigned long)secondInt];
        }
        _timeStr = [NSString stringWithFormat:@"%@:%@", minuteStr, secondStr];
        
        //单位KM or ML
        NSString* kmStr = [WWDTools stringFromIndexCount:6 count:2 from:value];
        if ([@"00" isEqual:kmStr]) {
            _speedDanWeiStr = @"KM/H";
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:SPEED_ELEMETNS_UNIT];
        }else if([@"01" isEqual:kmStr]){
            _speedDanWeiStr = @"ML/H";
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:SPEED_ELEMETNS_UNIT];
        }

        //speed 0.0
        NSString* speedStr = [WWDTools stringFromIndexCount:8 count:4 from:value];
        NSUInteger speedInt = [WWDTools intFromHexString:speedStr];
        float speedF = speedInt / 10.00;
        _speedStr = [NSString stringWithFormat:@"%0.1f", speedF];
//        NSTimer* timer = [NSTimer timerWithTimeInterval:0.1 target:self selector:@selector(biaoView) userInfo:nil repeats:NO];
//        [timer fire];
        [self rotateImageViewWithSpeed:[_speedStr floatValue]];
        _bSpeedF = speedF;
        
        //distance 0.00
        NSString* distanceStr = [WWDTools stringFromIndexCount:12 count:6 from:value];
        NSUInteger distanceInt = [WWDTools intFromHexString:distanceStr];
        float distanceF = distanceInt / 100.0;
        _distanceStr = [NSString stringWithFormat:@"%0.2f", distanceF];
        
        //calorie 0.0
        NSString* calorieStr = [WWDTools stringFromIndexCount:18 count:6 from:value];
        NSUInteger calorieInt = [WWDTools intFromHexString:calorieStr];
        float calorieF = calorieInt / 10.0;
        _calorieStr = [NSString stringWithFormat:@"%0.1f", calorieF];
        
        //odo 0.0
        NSString* odoStr = [WWDTools stringFromIndexCount:24 count:6 from:value];
        NSUInteger odoInt = [WWDTools intFromHexString:odoStr];
        float odoF = odoInt / 10.0;
        _odoStr = [NSString stringWithFormat:@"%0.1f", odoF];
        
        //rpm
        NSString* rpmStr = [WWDTools stringFromIndexCount:30 count:4 from:value];
        NSUInteger rpmInt = [WWDTools intFromHexString:rpmStr];
        _rpmStr = [NSString stringWithFormat:@"%lu", (unsigned long)rpmInt];
        
        //cnt
        NSString* cntStr = [WWDTools stringFromIndexCount:34 count:4 from:value];
        NSUInteger cntInt = [WWDTools intFromHexString:cntStr];
        _cntStr = [NSString stringWithFormat:@"%lu", (unsigned long)cntInt];
    }
    //心率
    else if([@"F9" isEqual:idString]){
        //heart rate value
        NSString* hrValueStr = [WWDTools stringFromIndexCount:2 count:4 from:value];
        NSUInteger hrValueInt = [WWDTools intFromHexString:hrValueStr];
        _heartRateStr = [NSString stringWithFormat:@"%lu", (unsigned long)hrValueInt];
        //heart rate set
        NSString* hrSetStr = [WWDTools stringFromIndexCount:6 count:4 from:value];
        NSUInteger hrSetInt = [WWDTools intFromHexString:hrSetStr];
        _heartRateXianStr = [NSString stringWithFormat:@"%lu", (unsigned long)hrSetInt];
    }
    // 倒计
    /*
    else  if ([@"FD" isEqual:idString]) {
        // time
        NSString* minuteStr = [NSString string];
        NSString* secondStr = [NSString string];
        NSString* minuteHexStr = [WWDTools stringFromIndexCount:2 count:2 from:value];
        NSString* secondHexStr = [WWDTools stringFromIndexCount:4 count:2 from:value];
        NSUInteger minuteInt = [WWDTools intFromHexString:minuteHexStr];
        NSUInteger secondInt = [WWDTools intFromHexString:secondHexStr];
        if (minuteInt < 10) {
            minuteStr = [NSString stringWithFormat:@"0%lu", (unsigned long)minuteInt];
        }else{
            minuteStr = [NSString stringWithFormat:@"%lu", (unsigned long)minuteInt];
        }
        if (secondInt < 10) {
            secondStr = [NSString stringWithFormat:@"0%lu", (unsigned long)secondInt];
        }else{
            secondStr = [NSString stringWithFormat:@"%lu", (unsigned long)secondInt];
        }
        _timeStr = [NSString stringWithFormat:@"%@:%@", minuteStr, secondStr];
        
        // danwei
        NSString* modeStr = [WWDTools stringFromIndexCount:6 count:2 from:value];
        if ([@"00" isEqual:modeStr]) {
            _speedDanWeiStr = @"KM/H";
        }else if([@"01" isEqual:modeStr]){
            _speedDanWeiStr = @"ML/H";
        }
        
        //dist 0.00
        NSString* distHexStr = [WWDTools stringFromIndexCount:8 count:6 from:value];
        NSUInteger distInt = [WWDTools intFromHexString:distHexStr];
        float distF = distInt / 100.0;
        _distanceStr = [NSString stringWithFormat:@"%0.2f", distF];
        
        //calorie 0.0
        NSString* calorieHexStr = [WWDTools stringFromIndexCount:14 count:6 from:value];
        NSUInteger calorieInt = [WWDTools intFromHexString:calorieHexStr];
        float calorieF = calorieInt / 10.0;
        _calorieStr = [NSString stringWithFormat:@"%0.1f", calorieF];
        
        //cnt
        NSString* cntHexStr = [WWDTools stringFromIndexCount:20 count:4 from:value];
        NSUInteger cntInt = [WWDTools intFromHexString:cntHexStr];
        _cntStr = [NSString stringWithFormat:@"%u", (unsigned int)cntInt];
    }
    */
    [self resetValue];
}

- (void)viewWillAppear:(BOOL)animated{
//    [self rotateImageView:_zhenImage withAngle:-30];
    [super viewWillAppear:animated];
    if (_myAppDelegate.bleConnectStatus) {
        [_connBtn setImage:_connectedBleImage];
    }else{
        [_connBtn setImage:_connBleImage];
    }
    WMSpeedElements* setElements = [[WMSpeedElements alloc] init];
    _mHRSetLabel.text = [setElements mHR];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"mysportCell" forIndexPath:indexPath];
    UILabel* name1 = (UILabel*)[tableView viewWithTag:1];
    UILabel* value1 = (UILabel*)[tableView viewWithTag:2];
    UILabel* name2 = (UILabel*)[tableView viewWithTag:3];
    UILabel* value2 = (UILabel*)[tableView viewWithTag:4];
    NSInteger rowNo = indexPath.row;
    switch (rowNo) {
        case 0:
        {
            name1.text = @"心率";
            name2.text = @"心率限定";
            value1.text = _heartRateStr;
            WMSpeedElements* setElements = [[WMSpeedElements alloc] init];
            value2.text = setElements.mHR;
            _mHRSetLabel = value2;
            break;
        }
        case 1:
        {
            name1.text = @"时间";
            name2.text = @"卡路里";
            value1.text = _timeStr;
            value2.text = _calorieStr;
            break;
        }
        case 2:
        {
            name1.text = @"里程";
            name2.text = @"总里程";
            value1.text = _distanceStr;
            value2.text = _odoStr;
            break;
        }
        case 3:
        {
            name1.text = @"转速";
            name2.text = @"圈数";
            value1.text = _rpmStr;
            value2.text = _cntStr;
            break;
        }
        default:
            break;
    }
    return cell;
}

//根据传入的imageView及角度来旋转imageView
- (void)rotateImageView:(UIImageView *)imageView withAngle:(CGFloat)angle{
//    CGFloat width = imageView.bounds.size.width;
//    CGFloat radius = width / 2;
    

    CGFloat everyAngle = M_PI / 180;
    CGFloat ra = everyAngle * angle; //根据传入的角度算出弧度
    
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform"];
    CATransform3D fromValue = imageView.layer.transform;
    anim.fromValue = [NSValue valueWithCATransform3D:fromValue];
    
    
    CATransform3D toValue = CATransform3DMakeRotation(ra, 0, 0, 1);
//    CATransform3D toValue = CATransform3DRotate(fromValue, ra, 0, 0, 1);
    anim.toValue = [NSValue valueWithCATransform3D:toValue];
    anim.duration = 0.5f;
    imageView.layer.transform = toValue;
    anim.removedOnCompletion = YES;
    [imageView.layer addAnimation:anim forKey:nil];
}

- (void)resetValue{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        _speedLabel.text = _speedStr;
//        _speedDanWeiLabel.text = _speedDanWeiStr;
//        [_mysportTableView reloadData];
//    });
}

//根据传入的速度来转
- (void)rotateImageViewWithSpeed:(float)speedF{
    float cha = speedF - _bSpeedF;
    float chaJue = fabsf(cha);
    float jiaoDu = speedF * 3.0f - 30;
    NSLog(@"cha : %0.1f", cha);
    if (speedF <= 85) {
        if (chaJue >= 60) {
            [self rotateImageView:_zhenImage withAngle:90];
            [self rotateImageView:_zhenImage withAngle:jiaoDu];
        }else{
            [self rotateImageView:_zhenImage withAngle:jiaoDu];
        }
    }else if(speedF > 85){
        [self rotateImageViewWithSpeed:85];
    }
}

- (CGFloat)angleFromSpeed:(CGFloat)speed {
    if (speed < 70) {
        return 210 - speed * 3;
    }
    return 3 * (speed - 70);
}
@end
