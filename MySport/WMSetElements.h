//
//  WMSetElements.h
//  MySport
//
//  Created by maginawin on 15/3/30.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "WWDTools.h"

#define SET_ELEMENTS_HEART_RATE @"setElementHeartRate"
#define SET_ELEMENTS_TIME @"setElementsTime"
#define SET_ELEMENTS_DISTANCE @"setElementsDistance"
#define SET_ELEMENTS_COUNT_STEPS @"setElementsCountSteps"
#define SET_ELEMENTS_CALORIE @"setElementsCalorie"
#define SET_ELEMENTS_SET_MODE @"setElementsSetMode"
#define SET_MODE_DISTANCE_UNIT @"setModeDistanceUnit"

#define SET_ELEMENTS_HAS_DEFAULT_VALUES @"setElementsHasDefalutValues"

typedef NS_ENUM(NSInteger, SET_MODE) {
    SET_MODE_SPEED = 0,
    SET_MODE_PEDO
};

@interface WMSetElements : NSObject

@property (strong, nonatomic) NSString* heartRate;
@property (strong, nonatomic) NSString* time;
@property (strong, nonatomic) NSString* distance;
@property (strong, nonatomic) NSString* countSteps;
@property (strong, nonatomic) NSString* calorie;
@property (nonatomic) SET_MODE setMode;
@property (nonatomic) NSInteger distanceUnit;

@property (weak, nonatomic) AppDelegate* appDelegate;

- (void)writeSetElementsToPeripheral;

- (NSString*)upHeartRate;
- (NSString*)downHeartRate;
- (NSString*)upTime;
- (NSString*)downTime;
- (NSString*)upDistance;
- (NSString*)downDistance;
- (NSString*)upCountSteps;
- (NSString*)downCountSteps;
- (NSString*)upCalorie;
- (NSString*)downCalorie;
- (void)updateDistanceUnit:(NSInteger)distanceUnit;
- (NSString*)defaultDistanceUnit;

@end
