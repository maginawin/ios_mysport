//
//  WMSpeedTableViewController.m
//  MySport
//
//  Created by maginawin on 15/3/31.
//  Copyright (c) 2015年 mycj.wwd. All rights reserved.
//

#import "WMSpeedTableViewController.h"

@interface WMSpeedTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *mTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *mDistLabel;
@property (weak, nonatomic) IBOutlet UILabel *mCalorieLabel;
@property (weak, nonatomic) IBOutlet UILabel *mHRLabel;
@property (weak, nonatomic) IBOutlet UILabel *mDistUnitLabel;

@property (nonatomic) BOOL isLongPress;
@property (nonatomic) CGFloat repeatInterval;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *upDownButotns;

@end

@implementation WMSpeedTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] init];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    // 添加长按事件
    for (int i = 0; i < _upDownButotns.count; i++) {
        UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPressGesture.minimumPressDuration = 1.0;
        [_upDownButotns[i] addGestureRecognizer:longPressGesture];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _speedElements = [[WMSpeedElements alloc] init];
    _isLongPress = NO;
    _repeatInterval = 0.5;
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self setupInitValues];
    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self setupInitValues];
//    });
}

- (void)setupInitValues {
    _mTimeLabel.text = _speedElements.mTime;
    _mDistLabel.text = _speedElements.mDist;
    _mCalorieLabel.text = _speedElements.mCalorie;
    _mHRLabel.text = _speedElements.mHR;
    _mDistUnitLabel.text = _speedElements.mUnit;
//    NSLog(@"%@, %@, %@, %@, %@", _speedElements.mTime, _speedElements.mDist, _speedElements.mCalorie, _speedElements.mHR, _speedElements.mUnit);
}

- (void)longPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        _isLongPress = YES;
        NSInteger senderTag = sender.view.tag;
        [self longPressChangeViewWithTag:senderTag];
        NSLog(@"began");
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        _isLongPress = NO;
        _repeatInterval = 0.05;
    }
}

- (void)longPressChangeViewWithTag:(NSInteger)senderTag {
    if (_isLongPress) {
        dispatch_queue_t repeatSpeed = dispatch_queue_create("WMSpeed.repeawtSpeed", DISPATCH_QUEUE_SERIAL);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_repeatInterval * NSEC_PER_SEC)), repeatSpeed, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (senderTag == 12) {
                    _repeatInterval = 0.05;
                    _mTimeLabel.text = [_speedElements downTime];
                } else if (senderTag == 14) {
                    _repeatInterval = 0.05;
                    _mTimeLabel.text = [_speedElements upTime];
                } else if (senderTag == 22) {
                    _repeatInterval = 0.002;
                    _mDistLabel.text = [_speedElements downDist];
                } else if (senderTag == 24) {
                    _repeatInterval = 0.002;
                    _mDistLabel.text = [_speedElements upDist];
                } else if (senderTag == 32) {
                    _repeatInterval = 0.02;
                    _mCalorieLabel.text = [_speedElements downCalorie];
                } else if (senderTag == 34) {
                    _repeatInterval = 0.02;
                    _mCalorieLabel.text = [_speedElements upCalorie];
                } else if (senderTag == 42) {
                    _repeatInterval = 0.05;
                    _mHRLabel.text = [_speedElements downHR];
                } else if (senderTag == 44) {
                    _repeatInterval = 0.05;
                    _mHRLabel.text = [_speedElements upHR];
                }

            });
            [self longPressChangeViewWithTag:senderTag];
        });
    }
}

#pragma mark - IBAction

- (IBAction)downTime:(id)sender {
    _mTimeLabel.text = [_speedElements downTime];
}

- (IBAction)upTime:(id)sender {
    _mTimeLabel.text = [_speedElements upTime];
}

- (IBAction)downDist:(id)sender {
    _mDistLabel.text = [_speedElements downDist];
}

- (IBAction)upDist:(id)sender {
    _mDistLabel.text = [_speedElements upDist];
}

- (IBAction)downCalorie:(id)sender {
    _mCalorieLabel.text = [_speedElements downCalorie];
}

- (IBAction)upCalorie:(id)sender {
    _mCalorieLabel.text = [_speedElements upCalorie];
}

- (IBAction)downHR:(id)sender {
    _mHRLabel.text = [_speedElements downHR];
}

- (IBAction)upHR:(id)sender {
    _mHRLabel.text = [_speedElements upHR];
}

- (IBAction)sendElements:(id)sender {
    [_speedElements writeElementsToPeripheral];
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"正在设置..." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    [alert show];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        alert.message = @"设置完成";
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissWithClickedButtonIndex:0 animated:YES];
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
//    [cell setPreservesSuperviewLayoutMargins:NO];
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
